package br.com.mastertech.imersivo.pagamento;

public class Pagamento {
	
	private int id;
	
	private Cartao cartaoId;
	
	private String descricao;
	
	private double valor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cartao getCartaoId() {
		return cartaoId;
	}

	public void setCartaoId(Cartao cartaoId) {
		this.cartaoId = cartaoId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
