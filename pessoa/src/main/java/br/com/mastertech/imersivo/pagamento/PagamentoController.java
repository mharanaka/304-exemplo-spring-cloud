package br.com.mastertech.imersivo.pagamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PagamentoController {
	
	@Autowired
	private PagamentoService pagamentoService;
	
	@GetMapping("/pagamento/{cartao_id}")
	public Pagamento criaPagamento(@PathVariable int cartaoId) {
		return pagamentoService.criaPagamento(cartaoId);
	}

}
