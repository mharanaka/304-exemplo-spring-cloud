package br.com.mastertech.imersivo.pagamento;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {

	@GetMapping("/cartao/{numero}")
	public Cartao criacartao(@PathVariable int numero);

}
