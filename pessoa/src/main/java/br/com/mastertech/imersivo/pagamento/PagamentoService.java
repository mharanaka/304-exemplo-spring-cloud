package br.com.mastertech.imersivo.pagamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
	
	@Autowired
	private CartaoClient carroClient;

	public Pagamento criaPessoa(String nome, String modelo) {
		Pagamento pessoa = new Pagamento();
		pessoa.setNome(nome);
		pessoa.setCarro(carroClient.criaCarro(modelo));
		return pessoa;
	}
	
}
