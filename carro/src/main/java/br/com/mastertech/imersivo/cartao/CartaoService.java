package br.com.mastertech.imersivo.cartao;

import org.springframework.stereotype.Service;

@Service
public class CartaoService {

	public Cartao novoCartao(int id, int numero) {
		Cartao cartao = new Cartao();
		cartao.setId(id);
		cartao.setNumero(numero);
		return cartao;
	}
}
