package br.com.mastertech.imersivo.cartao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;

	@GetMapping("/cartao/{numero}")
	public Cartao novoCartao(@PathVariable int id, int numero) {
		return cartaoService.novoCartao(id, numero);
	}
}
